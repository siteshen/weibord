# -*- coding: utf-8 -*-

from datetime import datetime
from decorator import decorator
from jinja2 import Environment, FileSystemLoader
from rauth.service import OAuth2Service
from urllib import urlencode
from urlparse import parse_qsl
import constants
import os
import re
import redis
import simplejson
import web

import logging

log = logging.getLogger(__name__)

redis_instance = redis.Redis()


# ==================== weibo client ====================
class APIError(StandardError):
    '''
    raise APIError if got failed json message.
    '''
    def __init__(self, error_code, error, request):
        self.error_code = error_code
        self.error = error
        self.request = request
        StandardError.__init__(self, error)

    def __str__(self):
        return 'APIError: %s: %s, request: %s' % (self.error_code, self.error, self.request)


class WeiboClient(OAuth2Service):
    def __init__(self, access_token=None, uid=None):
        access_token = access_token or web.cookies().get('token');
        uid = uid or web.cookies().get('uid')

        self.api_url = 'https://api.weibo.com/2/%s.json'
        self.uid = uid

        super(WeiboClient, self).__init__(*constants.WEIBO_PARAMS, access_token=access_token)

    def api(self, api, expired_time=300, **params):
        key = 'weibord_api:%s:%s:%s' % (self.access_token, api, tuple(params.items()))
        resp = simplejson.loads(redis_instance.get(key) or '""')

        if resp:
            log.debug('cache HITS for %s' % key)
            return resp

        # authorized use access_token, unauthorized use {'source': consumer_key}
        if self.access_token:
            params.update({'access_token': self.access_token})
        else:
            params.update({'source': self.consumer_key})

        resp = self.get(self.api_url % api, params=params).content
        if type(resp) == dict and resp.get('error_code'):
            raise APIError(resp.get('error_code'),
                           resp.get('error'),
                           resp.get('request'))

        log.debug('cache MISS for %s' % key)
        redis_instance.setex(key, simplejson.dumps(resp), expired_time)
        return resp

    def api_po(self, api, **params):
        # authorized use access_token, unauthorized use {'source': consumer_key}
        if self.access_token:
            params.update({'access_token': self.access_token})
        else:
            params.update({'source': self.consumer_key})

        resp = self.post(self.api_url % api, data=params).content
        if resp.get('error_code'):
            raise APIError(resp.get('error_code'),
                           resp.get('error'),
                           resp.get('request'))
        return resp

    def get_user(self, uid=None, screen_name=None):
        if uid:
            return self.api('users/show', uid=uid)
        elif screen_name:
            return self.api('users/show', screen_name=screen_name)
        else:
            return self.api('users/show', uid=self.uid)

    def expand_short_url(self, url_short):
        key = 'weibord_short_url:%s' % url_short
        url = redis_instance.get(key)
        if url:
            log.debug('cache HITS for %s' % key)
            result = simplejson.loads(url)
        else:
            log.debug('cache MISS for %s' % key)
            resp = self.api('short_url/expand', url_short=url_short)['urls'][0]
            redis_instance.set(key, simplejson.dumps(resp))
            result = resp
        return result

    def shorten_long_url(self, url_long):
        key = 'weibord_long_url:%s' % url_long
        url = redis_instance.get(key)
        if url:
            log.debug('cache HITS for %s' % key)
            result = simplejson.loads(url)
        else:
            log.debug('cache MISS for %s' % key)
            resp = self.api('short_url/shorten', url_long=url_long)['urls'][0]
            redis_instance.set(key, simplejson.dumps(resp))
            result = resp
        return result

    def get_face(self, emotion):
        key = 'weibord_faces'
        faces = redis_instance.get(key)
        if faces:
            log.debug('cache HITS for %s' % key)
            emotions = simplejson.loads(faces)
        else:
            log.debug('cache MISS for %s' % key)
            emotions = self.api('emotions')
            redis_instance.setex(key, simplejson.dumps(emotions), 86400)

        for e in emotions:
            if e['phrase'] == '[%s]' % emotion:
                return e
        return {}

# export these two usefull functions
def get_short_url(url):
    return WeiboClient().shorten_long_url(url).get('url_short', url)

def get_long_url(url):
    return WeiboClient().expand_short_url(url).get('url_long', url)

def get_face_url(emotion):
    return WeiboClient().get_face(emotion).get('url', None)
# ==================== weibo client ====================


# ==================== html renderer ====================
def update_jinja_env(env):

    @decorator
    def at_href(func, *args, **kwargs):
        text = func(*args, **kwargs)
        user_href = ' <a class="user" href="/u/%s">%s</a> '
        return re.sub(
            '@((?u)[\w-]+)',
            lambda obj: user_href % (obj.group(0), obj.group(0)),
            text)

    @decorator
    def at_url(func, *args, **kwargs):
        text = func(*args, **kwargs)
        url_href = ' <a class="url" target="_blank" href="%s">%s</a> '
        return re.sub(
            'http://t.cn/[\w-]+',
            lambda obj: url_href % (get_long_url(obj.group(0)), obj.group(0)),
            text)

    @decorator
    def at_topic(func, *args, **kwargs):
        text = func(*args, **kwargs)
        topic_href = ' <a class="topic" target="_blank" href="http://huati.weibo.com/k/%s">%s</a> '
        return re.sub(
            '#(?u)[\w-]+#',
            lambda obj: topic_href % (obj.group(0)[1:-1], obj.group(0)),
            text)

    @decorator
    def at_face(func, *args, **kwargs):
        text = func(*args, **kwargs)
        emotion_src = ' <img src="%s" alt="%s" title="%s" />'
        def replace(match):
            emotion = match.group(1)
            face_url = get_face_url(emotion)
            return face_url and emotion_src % (face_url, emotion, emotion) or emotion
        return re.sub(
            '\[([^\[\]]+)\]',
            replace,
            text)


    @at_url
    @at_href
    @at_topic
    @at_face
    def weibo_text(text):
        return text


    def next_midnight():
        now = datetime.now()
        if now.hour == 0 and now.minute == 0 and now.second == 0:
            return now
        else:
            return now.replace(day=now.day + 1, hour=0, minute=0, second=0)


    def format_time(created_at):
        dt = datetime.strptime(created_at, '%a %b %d %X +0800 %Y')
        now = datetime.now()
        delta = now - dt

        minutes_ago = delta.seconds / 60
        hours_ago = delta.seconds / 60 / 60
        days_ago = (next_midnight() - dt).days

        if days_ago > 2:
            datetime.strftime(dt, '%m-%d %H:%M')
        if days_ago == 2:
            return u'前天 %s' % datetime.strftime(dt, '%H:%M')
        if days_ago == 1:
            return u'昨天 %s' % datetime.strftime(dt, '%H:%M')
        if days_ago == 0:
            if hours_ago == 0:
                return u'%s 分钟前' % minutes_ago
            elif minutes_ago == 0:
                return u'刚刚'
            else:
                return u'今天 %s' % datetime.strftime(dt, '%H:%M')


    env.filters.update({'weibo_text': weibo_text, 'format_time': format_time})


def render_template(template_name, context):
    env = Environment(loader=FileSystemLoader(
            os.path.join(os.path.dirname(__file__), 'templates')))
    update_jinja_env(env)
    filename = '%s.jinja2' % template_name
    return env.get_template(filename).render(context)
# ==================== html renderer ====================


# ==================== misc utils ====================
def simple_parse_qs(qs, keep_blank_values=0, strict_parsing=0):
    '''
    like urlparse.parse_qs(), but return {'key1': value1, 'key2': 'value2'}
    intead of {'key1': [value1], 'key2': [value2]}
    '''
    dict = {}
    for name, value in parse_qsl(qs, keep_blank_values, strict_parsing):
        dict[name] = value
    return dict
# ==================== misc utils ====================


# ==================== client handler ====================
class Base:
    def __init__(self):
        self.context = web.ctx
        self.cookies = web.cookies()
        self.params = simple_parse_qs(self.context.get('query')[1:])
        self.data = simple_parse_qs(web.data())
        self.client = WeiboClient(self.cookies.get('token'), self.cookies.get('uid'))
        self.init_pagination()

    def init_pagination(self):
        self.page = int(self.params.get('page', 1))
        self.context['page'] = self.page

    def set_token_cookie(self, token=None):
        if token:
            expires = token.get('expires_in')
            web.setcookie('token', token.get('access_token'), expires=expires)
            web.setcookie('uid',   token.get('uid'),          expires=expires)

    def set_cookie_expired(self):
        web.setcookie('token', '', -1)
        web.setcookie('uid',   '', -1)

    def get_redirect_uri(self, name='weibo'):
        return 'http://%s/connect/%s' % (web.ctx.host, name)


class login(Base):
    def GET(self):
        url = self.client.get_authorize_url(redirect_uri=self.get_redirect_uri())
        return web.Found(url)


class connect(Base):
    def GET(self):

        if self.params.get('error'):
            return self.params.get('error_description')

        code = self.params.get('code')
        token = self.client.get_access_token(
            params={
                'redirect_uri': self.get_redirect_uri(),
                'code': code
                }).content

        if token.get('error'):
            return token.get('error_description')

        self.set_token_cookie(token)
        return web.Found('/')


class logout(Base):
    def GET(self):
        self.set_cookie_expired()
        return web.Found(self.params.get('next', '/'))


class home(Base):
    def GET(self):
        context = self.context

        try:
            context['user'] = self.client.get_user()
        except Exception:
            return web.Found('/login')

        resp = self.client.api('statuses/home_timeline', page=self.page)
        context['statuses'] = resp.get('statuses')
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class status_with_url(Base):
    def GET(self):
        context = self.context
        context['user'] = self.client.get_user()
        resp = self.client.api('statuses/home_timeline', page=self.page, count=200)
        context['statuses'] = filter(lambda s: re.search('(?u)http://t.cn/[\w-]+', s['text']),
                                     resp.get('statuses'))
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class user(Base):
    def GET(self, screen_name):
        context = self.context

        context['user'] = self.client.get_user(screen_name=screen_name)
        resp = self.client.api('statuses/user_timeline', uid=context['user']['id'], page=self.page)
        context['statuses'] = resp.get('statuses')
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class favorites(Base):
    def GET(self):
        context = self.context

        context['user'] = self.client.get_user()
        resp = self.client.api('favorites', page=self.page)
        context['statuses'] = [f.get('status') for f in resp.get('favorites')]
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class at_weibo(Base):
    def GET(self):
        context = self.context

        context['user'] = self.client.get_user()
        resp = self.client.api('statuses/mentions', page=self.page)
        context['statuses'] = resp.get('statuses')
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class at_comment(Base):
    def GET(self):
        context = self.context

        context['user'] = self.client.get_user()
        resp = self.client.api('comments/mentions', page=self.page)
        context['statuses'] = resp.get('comments')
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class retweet(Base):
    def GET(self):
        context = self.context
        context['user'] = self.client.get_user()
        resp = self.client.api('statuses/repost_by_me', page=self.page)
        context['statuses'] = resp.get('reposts')
        context['total_number'] = resp.get('total_number')
        return render_template('home', context)


class comments_create(Base):
    def POST(self):
        web.header('Content-Type', 'application/json')

        context = self.context
        context['user'] = self.client.get_user()
        id = self.data.get('id')
        comment = self.data.get('comment')
        if id and comment:
            self.client.api_po('comments/create', id=id, comment=comment)
            return simplejson.dumps({ 'status': 'success', 'message': u'评论成功' })
        else:
            return simplejson.dumps({ 'status': 'fail', 'message': u'评论内容不能为空' })


class statuses_repost(Base):
    def POST(self):
        web.header('Content-Type', 'application/json')

        context = self.context
        context['user'] = self.client.get_user()
        id = self.data.get('id')
        status = self.data.get('status', u'转发微博')
        if id and status:
            self.client.api_po('statuses/repost', id=id, status=status)
            return simplejson.dumps({ 'status': 'success', 'message': u'转发成功' })


class test(Base):
    def GET(self):
        return shorten_long_url('http://code.google.com/p/redis/wiki/CommandReference')
        return self.client.expand_short_url('http://t.cn/h4DwT1')
        return None
# ==================== client handler ====================


# ==================== url router ====================
urls = (

    '/', 'home',
    '/url', 'status_with_url',
    '/@/comment', 'at_comment',
    '/@/weibo', 'at_weibo',
    '/connect/weibo', 'connect',
    '/fav', 'favorites',
    '/login', 'login',
    '/login/weibo', 'login',
    '/logout', 'logout',
    '/rt', 'retweet',
    '/u/@((?u)[\w-]+)', 'user',
    '/test', 'test',
    '/comments/create', 'comments_create',
    '/statuses/repost', 'statuses_repost',

    )
# ==================== url router ====================


# ==================== logging config ====================
def config_logger():
    FORMAT = "%(asctime)s %(levelname)-5.5s [%(filename)s %(funcName)s %(lineno)d]: %(message)s"
    logging.basicConfig(format=FORMAT, level='DEBUG',
                        filename="weibord_access.log")
# ==================== logging config ====================


if __name__ == '__main__':
    config_logger()
    app = web.application(urls, globals(), autoreload=True)
    app.run()
